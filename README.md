# Lymphoma Patient Selection

## Description
This script is used to compare foton and proton Lymphoma plans based on DALROG(Danish Lymphoma Radiation Oncology Group)'s guidelines in order to figure out which type of treatment the patient should receive. All the original calculations and risk values can be fund in the documment: Background_Accumulated_Risk_LUT_vers4_lungonlysmokers.xlsx. All the calculations are based on the calculations in this document. 

## Language
- Script: English
- User interface: Danish

## Disclaimer
All use of this script is at **your own risk**. In order to commission this script for clinical use **it is recommended to test the script on a series of patient plans** to make sure all works as planned.

## Script type
This script is a esapi plug-in script, which means that the script should be run from within "External Beam Planning". The script does not have write permission, but ONLY read, so it does not need to be script approved in eclipse. 

## Version
- Current script version: 1.1.3.1

## Date last changed
23-05-2024

## Dependenci versions
- Esapi API version: 16.1
- .NET Framework: 4.6.1

## Tests
### Code review:

**Date:** 19-12-2022

**Participants:** Maria Fuglsang Jensen (physicist), Klaus Seiersen (physicist) and Asbjørn Christian Borgen (engineer).

**Comments:**
- [x] needs a description of abbreviations
- [x] needs readme with decription of tests and documentation of code review.
- [x] Add flow charts and charts in general.

### User tests:

**User:** Jakob Borup Thomsen (physicist), has developed the model the script is based on.

**Number of patients used on:** 4 (but Jakob has tried to change the input values and see what happened.)

**Result:** The script gives the expected results.

### Unit Tests:

**Total number of tests:** 120

**Classes tested:** RiskCalculator (37 tests), RiskCalculatorTotal (24 tests), RiskCalculatorExcess (23 tests) 
and CPRService (36 tests).

**Result of tests:** All passed.

**Description of tests:** Tests all the calculations of CHD, HVD, LC and BC with limit values. Only calculation methods has been Unit tested as they are the operations with the highest risk. Test the CPRService as the values are used to caluclate the risk values.

## Installation
Steps:

1. Download the source code for the script "LymphomaPatientSelection".
2. If your esapi is another version than 16.1, then you need to referance the correct version of the esapi API insted of the current as it is the 16.1 version. This can require some changes to the code if older versions of esapi are used.
3. Restore all referances to libaries and NuGet Packages.
4. Build the project in release mode.
5. Run the resulting exe file from eclipse "External Beam Planning".    

## Contact information
For help the author of the script can be contacted on this e-mail adresse:

- maikgu@rm.dk

## Authors and acknowledgment
This script is made by Maiken Guldberg (engineer) at DCPT. Jakob Borup Thomsen has been a great help in development of the script, choice of functionality and test of the script.  

## License
This project is under MIT license (see licens for more info).

