﻿using LymphomaPatientSelection.Models;
using LymphomaPatientSelection.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelectionTests.Tests
{
    [TestFixture]
    public class RiskCalculatorTotalTests
    {
        private RiskCalculatorTotal _uut;
        private List<LymphomaModelParameters> _parametersList;

        [SetUp]
        public void SetUp()
        {
            _parametersList = LymphomaModelParametersIO.ReadFile();
            _uut = new RiskCalculatorTotal(_parametersList);
        }

        [TestCase(-1,50,79,'m',double.NaN)]
        [TestCase(0, 50, 79, 'm', 0)]
        [TestCase(0, 50, 26, 'm', 106.396)]
        [TestCase(1, 50, 26, 'm', 138.314)]
        [TestCase(0, 50, 26, 'w', 62.746)]
        [TestCase(1, 50, 26, 'w', 81.570)]
        public void GetCHD_Tests(double CRF, double MHD, int age, char sex, double expResult)
        {
            double result = _uut.GetCHD(CRF, MHD, age, sex);

            Assert.AreEqual(Math.Round(expResult,3), Math.Round(result,3));
        }

        [TestCase(3, 3, 53, 18, 'm', 12.5)]
        [TestCase(3, 3, 79, 18, 'm', 0)]
        [TestCase(3, 4, 26, 18, 'm', 13.217)]
        [TestCase(3, 2, 0, 18, 'm', 13.360)]
        public void GetHVD_Tests(double MAHVD, double MMHVD, int age, int nFractions, char sex, double expResult)
        {
            double result = _uut.GetHVD(MAHVD, MMHVD, age, nFractions, sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }

        
        [TestCase(4,53,'m',17.986)]
        [TestCase(4, 53, 'w', 15.327)]
        [TestCase(4, 26, 'm', 23.460)]
        [TestCase(4, 26, 'w', 18.768)]
        [TestCase(4, 0, 'm', 23.460)]
        [TestCase(4, 0, 'w', 18.768)]
        public void GetLC_Tests(double MLD, int age, char sex, double expResult)
        {
            double result = _uut.GetLC(MLD,age,sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }

        [TestCase(2, 79, 'w', 0.0)]
        [TestCase(2, 53, 'w', 11.208)]
        [TestCase(3, 53, 'w', 12.495)]
        [TestCase(double.NaN, 53, 'm', double.NaN)]
        [TestCase(2, 26, 'w', 14.627)]
        [TestCase(double.NaN, 26, 'm', double.NaN)]
        [TestCase(2, 0, 'w', 14.674)]
        [TestCase(double.NaN, 0, 'm', double.NaN)]
        public void GetBC_Tests(double MBD, int age, char sex, double expResult)
        {
            double result = _uut.GetBC(MBD, age, sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }
    }
}
