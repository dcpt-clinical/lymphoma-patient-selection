﻿using LymphomaPatientSelection.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelectionTests.Tests
{
    [TestFixture]
    public class CPRServiceTests
    {
        private CPRService _uut;
        [SetUp]
        public void Setup()
        {
            _uut = new CPRService();
        }
        //All used numbers are not valid CPR numbers
        public void GetCPRClean_NotExact10Char_ExceptionWithCorrectMessage()
        {
            string id = "100861";
            string expectedErrorMessage = "Patient id'et er ikke et dansk CPR nummer.\nIndtast selv køn og alder for patient.";

            Exception ex = Assert.Throws<Exception>(delegate { _uut.GetCPRClean(id); });

            Assert.That(ex.Message, Is.EqualTo(expectedErrorMessage));
        }
        //All used numbers are not valid CPR numbers
        public void GetCPRClean_NotOlyNumbers_ExceptionWithCorrectMessage()
        {
            string id = "100861-xxxx";
            string expectedErrorMessage = "Patient id'et er ikke et dansk CPR nummer.\nIndtast selv køn og alder for patient.";

            Exception ex = Assert.Throws<Exception>(delegate { _uut.GetCPRClean(id); });

            Assert.That(ex.Message, Is.EqualTo(expectedErrorMessage));
        }
        //All used numbers are not valid CPR numbers
        public void GetCPRClean_PatientBirthDateIsNotDate_ExceptionWithCorrectMessage()
        {
            string id = "401861-0000";
            string expectedErrorMessage = "Patient id'et er ikke et dansk CPR nummer.\nIndtast selv køn og alder for patient.";

            Exception ex = Assert.Throws<Exception>(delegate { _uut.GetCPRClean(id); });

            Assert.That(ex.Message, Is.EqualTo(expectedErrorMessage));
        }
        //All used numbers are not valid CPR numbers
        public void GetCPRClean_ReturnsCleanedCPR()
        {
            string testId = "290261-0000";
            string expectedId = "2902610000";
            
            string actualId = _uut.GetCPRClean(testId);

            Assert.AreEqual(expectedId,actualId);
        }
        //All used numbers are not valid CPR numbers
        [TestCase("xxxx950xxx", "1995")] // 7 cif is >= 0 and <= 3
        [TestCase("xxxx951xxx", "1995")] // 7 cif is >= 0 and <= 3
        [TestCase("xxxx952xxx", "1995")] // 7 cif is >= 0 and <= 3
        [TestCase("xxxx953xxx", "1995")] // 7 cif is >= 0 and <= 3

        [TestCase("xxxx214xxx", "2021")] // 7 cif is 4 and year short is >= 0 and <= 36
        [TestCase("xxxx004xxx", "2000")] // 7 cif is 4 and year short is >= 0 and <= 36
        [TestCase("xxxx364xxx", "2036")] // 7 cif is 4 and year short is >= 0 and <= 36

        [TestCase("xxxx374xxx", "1937")] // 7 cif is 4 and year short is >= 37 and <= 99
        [TestCase("xxxx994xxx", "1999")] // 7 cif is 4 and year short is >= 37 and <= 99
        [TestCase("xxxx554xxx", "1955")] // 7 cif is 4 and year short is >= 37 and <= 99

        [TestCase("xxxx005xxx", "2000")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx575xxx", "2057")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx285xxx", "2028")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx007xxx", "2000")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx577xxx", "2057")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx287xxx", "2028")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx008xxx", "2000")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx578xxx", "2057")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57
        [TestCase("xxxx288xxx", "2028")] // 7 cif is >= 5 and <= 8 and year short is >= 0 and <= 57

        [TestCase("xxxx585xxx", "1858")] // 7 cif is >= 5 and <= 8 and year short is >= 58 and <= 99
        [TestCase("xxxx995xxx", "1899")] // 7 cif is >= 5 and <= 8 and year short is >= 58 and <= 99
        [TestCase("xxxx588xxx", "1858")] // 7 cif is >= 5 and <= 8 and year short is >= 58 and <= 99
        [TestCase("xxxx998xxx", "1899")] // 7 cif is >= 5 and <= 8 and year short is >= 58 and <= 99

        [TestCase("xxxx009xxx", "2000")] // 7 cif is 9 and year short is >= 0 and <= 36
        [TestCase("xxxx219xxx", "2021")] // 7 cif is 9 and year short is >= 0 and <= 36
        [TestCase("xxxx369xxx", "2036")] // 7 cif is 9 and year short is >= 0 and <= 36

        [TestCase("xxxx379xxx", "1937")] // 7 cif is 9 and year short is >= 37 and <= 99
        [TestCase("xxxx559xxx", "1955")] // 7 cif is 9 and year short is >= 37 and <= 99
        [TestCase("xxxx999xxx", "1999")] // 7 cif is 9 and year short is >= 37 and <= 99

        //Hard to test exception because it is not possible to get.
        public void GetYear_differentCases_returnCorrectYear(string id, string expResult)
        {
            string result = _uut.GetYear(id);

            Assert.AreEqual(expResult, result);
        }
        //All used numbers are not valid CPR numbers
        [TestCase("0912970xxx", 23)]
        [TestCase("1012970xxx", 24)]
        [TestCase("1112970xxx", 24)]
        [TestCase("1005110xxx", 109)]

        public void GetAge_differentCases_returnsTheCorrectAge(string id, int expResult)
        {
            var currentTimeHelperMock = new Mock<ICurrentTimeHelper>();
            currentTimeHelperMock.Setup(x => x.GetDateTimeNow()).Returns(new DateTime(2021, 12, 10));

            _uut.SetCurrentTimeHelper(currentTimeHelperMock.Object);

            int result = _uut.GetAge(id);

            Assert.AreEqual(expResult, result);
        }
        //All used numbers are not valid CPR numbers
        [TestCase("xxxxxxxxx3", 'm')]
        [TestCase("xxxxxxxxx4", 'w')]
        [TestCase("xxxxxxxxx0", 'w')]

        public void GetGender_differentCases_returnsTheCorrectGender(string id, char expResult)
        {
            char result = _uut.GetGender(id);

            Assert.AreEqual(expResult, result);
        }
    }
}
