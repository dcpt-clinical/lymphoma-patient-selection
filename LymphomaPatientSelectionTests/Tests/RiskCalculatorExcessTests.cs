﻿using LymphomaPatientSelection.Models;
using LymphomaPatientSelection.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelectionTests.Tests
{
    [TestFixture]
    public class RiskCalculatorExcessTests
    {
        private RiskCalculatorExcess _uut;
        private List<LymphomaModelParameters> _parametersList;

        [SetUp]
        public void SetUp()
        {
            _parametersList = LymphomaModelParametersIO.ReadFile();
            _uut = new RiskCalculatorExcess(_parametersList);
        }

        [TestCase(-1, 50, 79, 'm', double.NaN)]
        [TestCase(0, 50, 79, 'm', 0)]
        [TestCase(0, 50, 26, 'm', 82.752)]
        [TestCase(1, 50, 26, 'm', 114.671)]
        [TestCase(0, 50, 26, 'w', 48.803)]
        [TestCase(1, 50, 26, 'w', 67.626)]
        public void GetCHD_Tests(double CRF, double MHD, int age, char sex, double expResult)
        {
            double result = _uut.GetCHD(CRF, MHD, age, sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }

        [TestCase(3, 3, 79, 18, 'm', 0.0)]
        [TestCase(3, 4, 53, 18, 'm', 0.214)]
        [TestCase(3, 3, 26, 18, 'm', 0.158)]
        [TestCase(3, 2, 0, 18, 'm', 0.160)]
        public void GetHVD_Tests(double MAHVD, double MMHVD, int age, int nFractions, char sex, double expResult)
        {
            double result = _uut.GetHVD(MAHVD, MMHVD, age, nFractions, sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }


        [TestCase(40, 53, 'm', 64.860)]
        [TestCase(40, 53, 'w', 55.272)]
        [TestCase(40, 26, 'm', 84.600)]
        [TestCase(40, 26, 'w', 67.680)]
        [TestCase(40, 0, 'm', 84.600)]
        [TestCase(40, 0, 'w', 67.680)]
        public void GetLC_Tests(double MLD, int age, char sex, double expResult)
        {
            double result = _uut.GetLC(MLD, age, sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }

        [TestCase(30, 79, 'w', 0)]
        [TestCase(30, 53, 'w', 38.598)]
        [TestCase(double.NaN, 53, 'm', double.NaN)]
        [TestCase(30, 26, 'w', 50.372)]
        [TestCase(double.NaN, 26, 'm', double.NaN)]
        [TestCase(30, 0, 'w', 50.533)]
        [TestCase(double.NaN, 0, 'm', double.NaN)]
        public void GetBC_Tests(double MBD, int age, char sex, double expResult)
        {
            double result = _uut.GetBC(MBD, age, sex);

            Assert.AreEqual(Math.Round(expResult, 3), Math.Round(result, 3));
        }
    }
}
