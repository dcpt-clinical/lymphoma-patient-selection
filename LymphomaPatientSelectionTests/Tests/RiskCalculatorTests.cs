﻿using LymphomaPatientSelection.Models;
using LymphomaPatientSelection.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelectionTests.Tests
{
    [TestFixture]
    public class RiskCalculatorTests
    {
        private RiskCalculator _uut;
        private List<LymphomaModelParameters> _parametersList;

        [SetUp]
        public void SetUp()
        {
            _parametersList = LymphomaModelParametersIO.ReadFile();

            _uut = new RiskCalculator(_parametersList);
        }

        [TestCase(80,'m',"CHD")]
        [TestCase(100, 'm', "CHD")]
        [TestCase(-1, 'm', "CHD")]
        public void GetParameter_AgeNotInList_ReturnsNaN(int age, char sex, string parameter)
        {
            double result = _uut.GetParameter(age, sex, parameter);

            Assert.IsNaN(result);
        }

        [TestCase(79, 'm', "CHD",0)]
        [TestCase(79, 'w', "CHD", 0)]
        [TestCase(79, 'm', "HVD", 0)]
        [TestCase(79, 'w', "HVD", 0)]
        [TestCase(79, 'm', "LC", 0)]
        [TestCase(79, 'w', "LC", 0)]
        [TestCase(79, 'm', "BC", Double.NaN)]
        [TestCase(79, 'w', "BC", 0)]

        [TestCase(53, 'm', "CHD", 16.59596774)]
        [TestCase(53, 'w', "CHD", 9.787365591)]
        [TestCase(53, 'm', "HVD", 12.35)]
        [TestCase(53, 'w', "HVD", 12.35)]
        [TestCase(53, 'm', "LC", 11.5)]
        [TestCase(53, 'w', "LC", 9.8)]
        [TestCase(53, 'm', "BC", Double.NaN)]
        [TestCase(53, 'w', "BC", 8.635)]

        [TestCase(26, 'm', "CHD", 23.64349636)]
        [TestCase(26, 'w', "CHD", 13.94360042)]
        [TestCase(26, 'm', "HVD", 12.992)]
        [TestCase(26, 'w', "HVD", 12.992)]
        [TestCase(26, 'm', "LC", 15)]
        [TestCase(26, 'w', "LC", 12)]
        [TestCase(26, 'm', "BC", Double.NaN)]
        [TestCase(26, 'w', "BC", 11.269)]

        [TestCase(0, 'm', "CHD", 24.90967742)]
        [TestCase(0, 'w', "CHD", 14.69032258)]
        [TestCase(0, 'm', "HVD", 13.2)]
        [TestCase(0, 'w', "HVD", 13.2)]
        [TestCase(0, 'm', "LC", 15)]
        [TestCase(0, 'w', "LC", 12)]
        [TestCase(0, 'm', "BC", Double.NaN)]
        [TestCase(0, 'w', "BC", 11.305)]
        public void GetParameter_AgeIsInList_ReturnsCorrectParameters(int age, char sex, string parameter, double expResults)
        {
            double result = _uut.GetParameter(age, sex, parameter);

            Assert.AreEqual(expResults, result);
        }

        [TestCase(26, 'm', "LT")]
        [TestCase(26, 'n', "CHD")]
        public void GetParameter_ParameterOrSexNotCorrect_ReturnsNaN(int age, char sex, string parameter)
        {
            double result = _uut.GetParameter(age, sex, parameter);

            Assert.IsNaN(result);
        }
    }
}
