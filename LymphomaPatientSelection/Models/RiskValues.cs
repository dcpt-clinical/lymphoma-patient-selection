﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Models
{
    public class RiskValues
    {
        public string PlanType { get; set; }
        public double CHD { get; set; }
        public double HVD { get; set; }
        public double LC { get; set; }
        public double BC { get; set; }
    }
}
