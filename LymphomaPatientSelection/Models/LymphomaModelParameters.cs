﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Models
{
    public class LymphomaModelParameters
    {
        public int Age { get; set; }
        public double CHDmen { get; set; }
        public double CHDwomen { get; set; }
        public double HVD { get; set; }
        public double LCmen { get; set; }
        public double LCwomen { get; set; }
        public double BC { get; set; }
    }
}
