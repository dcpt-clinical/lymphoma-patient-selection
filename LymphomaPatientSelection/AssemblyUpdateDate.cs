﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyUpdateDate : Attribute
    {
        public string Value { get; private set; }

        public AssemblyUpdateDate() : this("") { }
        public AssemblyUpdateDate(string value) { Value = value; }
    }
}
