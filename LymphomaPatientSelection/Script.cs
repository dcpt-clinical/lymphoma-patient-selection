﻿using LymphomaPatientSelection.Models;
using LymphomaPatientSelection.Services;
using LymphomaPatientSelection.ViewModels;
using LymphomaPatientSelection.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.TPS.Common.Model.API;

namespace VMS.TPS
{
    public class Script
    {
        public Script() 
        {

        }
        public void Execute(ScriptContext context)
        {
            var esapiService = new EsapiService(context);

            MainWindow window = new MainWindow();

            IRiskCalculator totalRisk = new RiskCalculatorTotal(LymphomaModelParametersIO.ReadFile());
            IRiskCalculator excessRisk = new RiskCalculatorExcess(LymphomaModelParametersIO.ReadFile());
            ICPRService cprService = new CPRService();
            IMessageBoxService messageBoxService = new MessageBoxService();

            var viewModel = new ViewModel(totalRisk, excessRisk, esapiService, cprService, messageBoxService);
            window.DataContext = viewModel;
            window.ShowDialog();
        }
    }
}
