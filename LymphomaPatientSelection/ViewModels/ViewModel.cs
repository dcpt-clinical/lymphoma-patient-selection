﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LymphomaPatientSelection.Models;
using LymphomaPatientSelection.Services;
using Prism.Mvvm;
using Prism.Commands;
using Prism.Services.Dialogs;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace LymphomaPatientSelection.ViewModels
{
    public class ViewModel : BindableBase
    {
        #region Plan props
        private List<string> _plans = new List<string>();
        public List<string> Plans
        {
            get => _plans;
            set => SetProperty(ref _plans, value);
        }
        private string _selectedfotonPlan = "";
        public string SelectedFotonPlan
        {
            get => _selectedfotonPlan;
            set => SetProperty(ref _selectedfotonPlan, value);
        }

        private string _selectedProtonPlan = "";
        public string SelectedProtonPlan
        {
            get => _selectedProtonPlan;
            set => SetProperty(ref _selectedProtonPlan, value);
        }
        #endregion

        #region Choices
        private List<string> _gender = new List<string>() { "Mand", "Kvinde","Ikke binær" };
        public List<string> Gender
        {
            get => _gender;
            set => SetProperty(ref _gender, value);
        }
        private string _selectedGender = "";
        public string SelectedGender
        {
            get => _selectedGender;
            set => SetProperty(ref _selectedGender, value);
        }
        private List<int> _crfChoises = new List<int>() { 0, 1 };
        public List<int> CRFChoises
        {
            get => _crfChoises;
            set => SetProperty(ref _crfChoises, value);
        }
        private int _selectedCRF = -1;
        public int SelectedCRF
        {
            get => _selectedCRF;
            set => SetProperty(ref _selectedCRF, value);
        }
        private int _patientAge = -1;
        public int PatientAge
        {
            get => _patientAge;
            set => SetProperty(ref _patientAge, value);
        }
        private int _numberOfFractions = 0;
        public int NumberOfFractions
        {
            get => _numberOfFractions;
            set => SetProperty(ref _numberOfFractions, value);
        }
        #endregion

        #region Structures Foton
        private List<string> _structuresFoton = new List<string>();
        public List<string> StructuresFoton
        {
            get => _structuresFoton;
            set => SetProperty(ref _structuresFoton, value);
        }
        private string _heartFoton = "N/A";
        public string HeartFoton
        {
            get => _heartFoton;
            set => SetProperty(ref _heartFoton, value);
        }
        private string _lungsFoton = "N/A";
        public string LungsFoton
        {
            get => _lungsFoton;
            set => SetProperty(ref _lungsFoton, value);
        }
        private string _breastFoton = "N/A";
        public string BreastFoton
        {
            get => _breastFoton;
            set => SetProperty(ref _breastFoton, value);
        }
        private string _aorticValveFoton = "N/A";
        public string AorticValveFoton
        {
            get => _aorticValveFoton;
            set => SetProperty(ref _aorticValveFoton, value);
        }
        private string _mitralValveFoton = "N/A";
        public string MitralValveFoton
        {
            get => _mitralValveFoton;
            set => SetProperty(ref _mitralValveFoton, value);
        }
        #endregion

        #region Structures Proton
        private List<string> _structuresProton = new List<string>();
        public List<string> StructuresProton
        {
            get => _structuresProton;
            set => SetProperty(ref _structuresProton, value);
        }
        private string _heartProton = "N/A";
        public string HeartProton
        {
            get => _heartProton;
            set => SetProperty(ref _heartProton, value);
        }
        private string _lungsProton = "N/A";
        public string LungsProton
        {
            get => _lungsProton;
            set => SetProperty(ref _lungsProton, value);
        }
        private string _breastProton = "N/A";
        public string BreastProton
        {
            get => _breastProton;
            set => SetProperty(ref _breastProton, value);
        }
        private string _aorticValveProton = "N/A";
        public string AorticValveProton
        {
            get => _aorticValveProton;
            set => SetProperty(ref _aorticValveProton, value);
        }
        private string _mitralValveProton = "N/A";
        public string MitralValveProton
        {
            get => _mitralValveProton;
            set => SetProperty(ref _mitralValveProton, value);
        }
        #endregion

        #region List of Risks
        private ObservableCollection<RiskValues> _totalRiskValues = new ObservableCollection<RiskValues>();
        public ObservableCollection<RiskValues> TotalRiskValues
        {
            get => _totalRiskValues;
            set => SetProperty(ref _totalRiskValues, value);
        }

        private ObservableCollection<RiskValues> _excessRiskValues = new ObservableCollection<RiskValues>();
        public ObservableCollection<RiskValues> ExcessRiskValues
        {
            get => _excessRiskValues;
            set => SetProperty(ref _excessRiskValues, value);
        }
        #endregion

        #region Commands
        public DelegateCommand StartCommand { get; private set; }
        public DelegateCommand LoadFotonStructuresCommand { get; private set; }
        public DelegateCommand LoadProtonStructuresCommand { get; private set; }
        public DelegateCommand EvaluateCommand { get; private set; }
        public DelegateCommand ExportCommand { get; private set; }
        public DelegateCommand ShowScriptInfoCommand { get; private set; }
        public DelegateCommand ShowShortCutMessageBoxCommand { get; private set; }
        #endregion

        #region Evaluate
        private bool _evaluateIsEnabled = true;
        public bool EvaluateIsEnabled 
        {
            get => _evaluateIsEnabled;
            set => SetProperty(ref _evaluateIsEnabled, value);
        }
        #endregion

        private IRiskCalculator _totalRiskCalculator;
        private IRiskCalculator _excessRiskCalculator;
        private IEsapiService _esapiService;
        private ICPRService _cprService;
        private IMessageBoxService _messageBoxService;

        public ViewModel(IRiskCalculator totalRiskCalculator, IRiskCalculator excessRiskCalculator, IEsapiService esapiService, ICPRService cprService, IMessageBoxService messageBoxService)
        {
            _totalRiskCalculator = totalRiskCalculator;
            _excessRiskCalculator = excessRiskCalculator;
            _esapiService = esapiService;
            _cprService = cprService;
            _messageBoxService = messageBoxService;
            SetUpCommands();
        }

        /// <summary>
        /// Set up delegate commands.
        /// </summary>
        private void SetUpCommands()
        {
            StartCommand = new DelegateCommand(Start);
            LoadFotonStructuresCommand = new DelegateCommand(LoadFotonStructures, LoadFotonStructuresCanExecute);
            LoadProtonStructuresCommand = new DelegateCommand(LoadProtonStructures, LoadProtonStructuresCanExecute);
            EvaluateCommand = new DelegateCommand(Evaluate);
            ExportCommand = new DelegateCommand(Export);
            ShowScriptInfoCommand = new DelegateCommand(ShowScriptInfo);
            ShowShortCutMessageBoxCommand = new DelegateCommand(ShowShortCutMessageBox);
        }

        private void Start()
        {
            //Gets the patients plans.
            Plans = _esapiService.GetPlans();
            //Get the id number of the patient
            string patientId = _esapiService.GetPatientId();

            try
            {
                //Cleans the patient id and sets the age of the patient depended on the CPR number.
                string cleanId = _cprService.GetCPRClean(patientId);
                PatientAge = _cprService.GetAge(patientId);
                SelectedGender = GetGender(patientId);
            }
            catch (Exception e)
            {
                _messageBoxService.ShowMessageBox(e.Message);
            }
        }

        /// <summary>
        /// Gets the gender of the patient form their id number.
        /// </summary>
        /// <param name="id">Patient id.</param>
        /// <returns>Patients gender.</returns>
        public string GetGender(string id)
        {
            string result = "ikke kendt";
            char gender = _cprService.GetGender(id);
            switch (gender)
            {
                case 'w':
                    result = "Kvinde";
                    break;
                case 'm':
                    result = "Mand";
                    break;
            }
            return result;
        }
        /// <summary>
        /// Foton structures can only be loaded if there has been selected a foton plan.
        /// </summary>
        /// <returns>Returns true if the foton plan has been selected.</returns>
        private bool LoadFotonStructuresCanExecute()
        {
            if(String.IsNullOrEmpty(SelectedFotonPlan) || String.IsNullOrWhiteSpace(SelectedFotonPlan))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Loads the structures for the foton plan and find the numbers of fractions.
        /// </summary>
        private void LoadFotonStructures()
        {
            ClearStructuresFoton();
            StructuresFoton = _esapiService.GetStructures(SelectedFotonPlan);
            StructuresFoton.OrderBy(s => s);
            StructuresFoton.Add("N/A");

            HeartFoton = SetChosenStructure(new List<string>() {"Heart"}, StructuresFoton);
            LungsFoton = SetChosenStructure(new List<string>() { "Lungs" }, StructuresFoton);
            if (SelectedGender == "Kvinde")
            {
                BreastFoton = SetChosenStructure(new List<string>() { "Breast" }, StructuresFoton);
            }
            else
            {
                BreastFoton = "N/A";
            }
            AorticValveFoton = SetChosenStructure(new List<string>() { "Aortic valve" }, StructuresFoton);
            MitralValveFoton = SetChosenStructure(new List<string>() { "Mitral valve" }, StructuresFoton);
        }
        /// <summary>
        /// Clear the selected foton structures.
        /// </summary>
        private void ClearStructuresFoton()
        {
            StructuresFoton.Clear();
            HeartFoton = "N/A";
            LungsFoton = "N/A";
            BreastFoton = "N/A";
            AorticValveFoton = "N/A";
            MitralValveFoton = "N/A";
        }
        /// <summary>
        /// Proton structures can only be loaded if there has been selected a proton plan.
        /// </summary>
        /// <returns>Returns true if the proton plan has been selected.</returns>
        private bool LoadProtonStructuresCanExecute()
        {
            if (String.IsNullOrEmpty(SelectedProtonPlan) || String.IsNullOrWhiteSpace(SelectedProtonPlan))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Loads the structures for the proton plan and find the numbers of fractions.
        /// </summary>
        private void LoadProtonStructures()
        {
            ClearStructuresProton();
            try
            {
                NumberOfFractions = _esapiService.GetNumberOfFractions(SelectedProtonPlan);
            }
            catch (Exception e)
            {
                _messageBoxService.ShowMessageBox(e.Message);
            }
            StructuresProton = _esapiService.GetStructures(SelectedProtonPlan);
            StructuresProton.OrderBy(s => s);
            StructuresProton.Add("N/A");
            
            HeartProton = SetChosenStructure(new List<string>() { "Heart" }, StructuresProton);
            LungsProton = SetChosenStructure(new List<string>() { "Lungs" }, StructuresProton);
            if(SelectedGender == "Kvinde")
            {
                BreastProton = SetChosenStructure(new List<string>() { "Breast" }, StructuresProton);
            }
            else
            {
                BreastProton = "N/A";
            }
            AorticValveProton = SetChosenStructure(new List<string>() { "Aortic valve" }, StructuresProton);
            MitralValveProton = SetChosenStructure(new List<string>() { "Mitral valve" }, StructuresProton);
        }
        /// <summary>
        /// Clear the selected proton structures.
        /// </summary>
        private void ClearStructuresProton()
        {
            NumberOfFractions = 0;
            StructuresProton.Clear();
            HeartProton = "N/A";
            LungsProton = "N/A";
            BreastProton = "N/A";
            AorticValveProton = "N/A";
            MitralValveProton = "N/A";
        }

        /// <summary>
        /// Checks is all needed data is set.
        /// </summary>
        /// <returns></returns>
        public bool CanCalculate()
        {
            if (HeartFoton == "N/A" || LungsFoton == "N/A" || AorticValveFoton == "N/A" || MitralValveFoton == "N/A")
            {
                _messageBoxService.ShowMessageBox("En eller flere strukture er ikke blevet valgt for fotoner.");
                return false;
            }
            else if (HeartProton == "N/A" || LungsProton == "N/A" || AorticValveProton == "N/A" || MitralValveProton == "N/A")
            {
                _messageBoxService.ShowMessageBox("En eller flere strukture er ikke blevet valgt for protoner.");
                return false;
            }
            else if (String.IsNullOrEmpty(SelectedGender) || String.IsNullOrWhiteSpace(SelectedGender) || SelectedGender == "ikke kendt")
            {
                _messageBoxService.ShowMessageBox("Patientens køn mangler.");
                return false;
            }
            else if(BreastFoton == "N/A" && SelectedGender == "Kvinde")
            {
                _messageBoxService.ShowMessageBox("Der er ikke valgt nogen bryst struktur for foton planen.");
                return false;
            }
            else if (BreastProton == "N/A" && SelectedGender == "Kvinde")
            {
                _messageBoxService.ShowMessageBox("Der er ikke valgt nogen bryst struktur for proton planen.");
                return false;
            }
            else if (SelectedCRF < 0)
            {
                _messageBoxService.ShowMessageBox("CRF kan ikke være negativ.");
                return false;
            }
            else if (PatientAge < 0)
            {
                _messageBoxService.ShowMessageBox("Patientens alder kan ikke være negativ.");
                return false;
            }
            else if (NumberOfFractions <= 0)
            {
                _messageBoxService.ShowMessageBox("Antallet af fraktioner kan ikke være <= 0.");
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Calculate the total and excess risks for the proton and the foton plan and adds them to the total risk values lists.
        /// </summary>
        private void Evaluate()
        {
            EvaluateIsEnabled = false;

            TotalRiskValues.Clear();
            ExcessRiskValues.Clear();

            if(CanCalculate() == true)
            {
                RiskValues fotonTotal = GetRisks("Foton", SelectedFotonPlan, _totalRiskCalculator, HeartFoton, LungsFoton, BreastFoton, AorticValveFoton, MitralValveFoton);
                RiskValues protonTotal = GetRisks("Proton", SelectedProtonPlan, _totalRiskCalculator, HeartProton, LungsProton, BreastProton, AorticValveProton, MitralValveProton);
                TotalRiskValues.Add(fotonTotal);
                TotalRiskValues.Add(protonTotal);

                RiskValues fotonExcess = GetRisks("Foton", SelectedFotonPlan, _excessRiskCalculator, HeartFoton, LungsFoton, BreastFoton, AorticValveFoton, MitralValveFoton);
                RiskValues protonExcess = GetRisks("Proton", SelectedProtonPlan, _excessRiskCalculator, HeartProton, LungsProton, BreastProton, AorticValveProton, MitralValveProton);
                ExcessRiskValues.Add(fotonExcess);
                ExcessRiskValues.Add(protonExcess);
            }
            EvaluateIsEnabled = true;
        }
        /// <summary>
        /// Gets the mean dose values for the different structures and calculate the excess or total risks for the given plan.
        /// </summary>
        /// <param name="planType">The type of the plan (Proton or Foton).</param>
        /// <param name="planId">The id of the plan.</param>
        /// <param name="riskCalculator">The given risk calculator (Excess or Total).</param>
        /// <param name="heartStructure">The heart structure.</param>
        /// <param name="lungsStructure">The lung structure.</param>
        /// <param name="breastStructure">The breast structure.</param>
        /// <param name="aorticValveStructure">The aortic valve structure.</param>
        /// <param name="mitralValveStructure">The mitral valve structure.</param>
        /// <returns>The risk values: CHD, HVD, LC and BC.</returns>
        private RiskValues GetRisks(string planType,string planId, IRiskCalculator riskCalculator, string heartStructure, string lungsStructure, string breastStructure, string aorticValveStructure, string mitralValveStructure)
        {
            double MHD = _esapiService.GetMeanDoseForStructure(heartStructure,planId, VMS.TPS.Common.Model.Types.DoseValue.DoseUnit.Gy);
            double MLD = _esapiService.GetMeanDoseForStructure(lungsStructure, planId, VMS.TPS.Common.Model.Types.DoseValue.DoseUnit.Gy);
            double MAHVD = _esapiService.GetMeanDoseForStructure(aorticValveStructure, planId, VMS.TPS.Common.Model.Types.DoseValue.DoseUnit.Gy);
            double MMHVD = _esapiService.GetMeanDoseForStructure(mitralValveStructure, planId, VMS.TPS.Common.Model.Types.DoseValue.DoseUnit.Gy);
            double MBD = Double.NaN;

            if (SelectedGender == "Kvinde")
            {
                MBD =  _esapiService.GetMeanDoseForStructure(breastStructure, planId, VMS.TPS.Common.Model.Types.DoseValue.DoseUnit.Gy);
            }

            char gender = GetGenderInChar();

            return new RiskValues()
            {
                PlanType = planType,
                CHD = riskCalculator.GetCHD(SelectedCRF, MHD, PatientAge, gender),
                HVD = riskCalculator.GetHVD(MAHVD, MMHVD, PatientAge, NumberOfFractions, gender),
                LC = riskCalculator.GetLC(MLD, PatientAge, gender),
                BC = riskCalculator.GetBC(MBD,PatientAge,gender)
            };
        }
        /// <summary>
        /// Convert the gender reprecentation from a string to a char.
        /// </summary>
        /// <returns>Gender as a char (w = woman, m = man).</returns>
        public char GetGenderInChar()
        {
            if(SelectedGender == "Kvinde")
            {
                return 'w';
            }
            else if(SelectedGender == "Mand")
            {
                return 'm';
            }
            else
            {
                return 'n';
            }
        }
        /// <summary>
        /// Export the risk values to a csv file.
        /// </summary>
        private void Export()
        {
            if(TotalRiskValues.Count != 0 && ExcessRiskValues.Count != 0)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                saveFileDialog.Title = "Vælg et sted at eksportere filen til og giv den et navn.";
                saveFileDialog.Filter = "csv files (*.csv)|*.csv";
                saveFileDialog.FilterIndex = 2;
                saveFileDialog.AddExtension = true;
                saveFileDialog.DefaultExt = ".csv";

                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string path = saveFileDialog.FileName;

                    RiskInfo riskInfo = new RiskInfo()
                    {
                        FotonPlan = SelectedFotonPlan,
                        ProtonPlan = SelectedProtonPlan,
                        PatientAge = PatientAge,
                        Gender = SelectedGender,
                        NumberOfFractions = NumberOfFractions,
                        CRF = SelectedCRF,
                        TotalRisk = TotalRiskValues.ToList(),
                        ExcessRisk = ExcessRiskValues.ToList()
                    };

                    try
                    {
                        ExportRisks.Export(path, riskInfo);

                        _messageBoxService.ShowMessageBox(String.Format("Risk resultaterne er blevet eksporteret til stien:\n{0}", path));
                    }
                    catch (Exception e)
                    {
                        _messageBoxService.ShowMessageBox(e.Message);
                    }
                }
            }
            else
            {
                _messageBoxService.ShowMessageBox("Total risk og excess risk skal være beregnet før data kan eksporteres.");
            }
            
        }

        /// <summary>
        /// Returns the name of the structure if the naming convention has been followed or else returns N/A and then the user need to choose the correct structure.
        /// </summary>
        /// <param name="searchString">Name of structure as by the naming convention</param>
        /// <param name="structurList">List of all the structures</param>
        /// <returns>Name of structure or N/A</returns>
        private string SetChosenStructure(List<string> searchStrings, List<string> structurList)
        {
            string result = "N/A";
            string search = structurList.Where(s => searchStrings.Any(st => s.ToUpper().Contains(st.ToUpper()))).FirstOrDefault();
            if (search != null)
            {
                result = search;
            }
            return result;
        }

        private void ShowScriptInfo()
        {
            string esapiVersion = "16.1";
            string model = "DALROG";
            string document = "Background Accumulated Risk LUT vers3";
            string firma = "Dansk Center for Partikelterapi (DCPT)";
            string creator = "Maiken H. Guldberg";
            AssemblyUpdateDate dateAttribute = (AssemblyUpdateDate)Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyUpdateDate));
            string date = dateAttribute.Value;
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            string text = String.Format("Lymfom tool til sammenligning af lymfom planer.\n\n" +
                "Beskrivelse: Esapi script til benyttelse ved sammenligninger af lymfom planer\n" +
                "ud fra {0}'s retningslinjer.\n\n" +
                "Disclaimer: Anvendelse af scriptet sker på eget ansvar.\n" +
                "Det anbefales at teste scriptet af på en række test patienten inden ibrugtagning.\n\n" +
                "Dokument: {6}\n" +
                "Esapi version: {1}\n" +
                "Versions dato: {2}\n" +
                "Script version: {3}\n" +
                "Udviklende center: {4}\n" +
                "Udvikler: {5}"
                , model, esapiVersion, date, version, firma, creator, document);

            _messageBoxService.ShowMessageBox(text);
        }

        private void ShowShortCutMessageBox()
        {
            string text = "Forkortelser:\n"+
                "CRF: Cardiac Risk Factors\n" +
                "CHD: Coronary Heart Disease\n" +
                "HVD: Heart Valve Disease\n" +
                "LC: Lung Cancer\n" +
                "BC: Breast Cancer";

            _messageBoxService.ShowMessageBox(text);
        }


    }
}
