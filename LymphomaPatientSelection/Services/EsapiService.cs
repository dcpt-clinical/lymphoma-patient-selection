﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using static VMS.TPS.Common.Model.Types.DoseValue;

namespace LymphomaPatientSelection.Services
{
    public class EsapiService : IEsapiService
    {
        private ScriptContext _context;
        public EsapiService(ScriptContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get the plans for the patient
        /// </summary>
        /// <returns>A list of plan id's</returns>
        public List<string> GetPlans()
        {
            List<string> plans = new List<string>();
            foreach (var plan in _context.IonPlansInScope)
            {
                plans.Add(plan.Id);
            }
            foreach (var plan in _context.ExternalPlansInScope)
            {
                plans.Add(plan.Id);
            }

            plans = plans.OrderBy(p => p).ToList();

            return plans;

        }
        /// <summary>
        /// Returns a plan setup if one is in focus.
        /// </summary>
        /// <returns>Id of plan.</returns>
        public string GetPlanInFocus()
        {
            string plan = "";
            if (_context.PlanSetup != null)
            {
                plan = _context.PlanSetup.Id;
            }
            return plan;
        }

        /// <summary>
        /// Get plan by plan id. Searches in IonPlansInScope and ExternalPlansInScope to find the plan.
        /// </summary>
        /// <param name="PlanId">The id of the plan.</param>
        /// <param name="IonPlansInScope">The ion-plans in scope.</param>
        /// <param name="ExternalPlansInScope">The external plans in scope.</param>
        /// <returns>The PlanSetup defined by the id.</returns>
        private PlanSetup GetPlan(string PlanId, IEnumerable<IonPlanSetup> IonPlansInScope, IEnumerable<ExternalPlanSetup> ExternalPlansInScope)
        {
            foreach (var item in IonPlansInScope)
            {
                if (item.Id == PlanId)
                {
                    return item;
                }
            }
            foreach (var item in ExternalPlansInScope)
            {
                if (item.Id == PlanId)
                {
                    return item;
                }
            }
            return null;

        }
        /// <summary>
        /// Finds all structures for the given plan.
        /// </summary>
        /// <param name="PlanId">The id of the plan</param>
        /// <returns>A list of id's of the structures for the plan.</returns>
        public List<string> GetStructures(string PlanId)
        {
            List<string> Structures = new List<string>();
            PlanSetup plan = GetPlan(PlanId, _context.IonPlansInScope, _context.ExternalPlansInScope);
            if (plan != null)
            {
                foreach (var structure in plan.StructureSet.Structures)
                {
                    Structures.Add(structure.Id);
                }
                Structures = Structures.OrderBy(s => s).ToList();
            }
            return Structures;

        }
        /// <summary>
        /// Get the id of the patient.
        /// </summary>
        /// <returns>The patient id as a string.</returns>
        public string GetPatientId()
        {
            return _context.Patient.Id;
        }
        /// <summary>
        /// Finds a structure by its id.
        /// </summary>
        /// <param name="ionPlansInScope">The ion-plans in scope.</param>
        /// <param name="externalPlansInScope">The external plans in scope.</param>
        /// <param name="planId">Id of the plan.</param>
        /// <param name="structureId">Id of the structure.</param>
        /// <returns>The structure given by the id.</returns>
        private Structure GetStructureById(IEnumerable<IonPlanSetup> ionPlansInScope, IEnumerable<ExternalPlanSetup> externalPlansInScope, string planId, string structureId)
        {
            return GetPlan(planId, ionPlansInScope, externalPlansInScope)?.StructureSet?.Structures?.FirstOrDefault(s => s.Id == structureId);
        }
        /// <summary>
        /// Gets DVH data for the plan by structure, dose value presentation and volume presentation. Bin width 0,001.
        /// </summary>
        /// <param name="ionPlansInScope">The ion-plans in scope.</param>
        /// <param name="externalPlansInScope">The external plans in scope.</param>
        /// <param name="planId">Id of the plan.</param>
        /// <param name="structure">The structure we need the DVH data from.</param>
        /// <param name="doseValuePresentation">The dose value representation [absolute,relativ]</param>
        /// <param name="volumePresentation">The volume presentation [absolute,relativ].</param>
        /// <returns>DVH data for the given structure.</returns>
        private DVHData GetDVHData(IEnumerable<IonPlanSetup> ionPlansInScope, IEnumerable<ExternalPlanSetup> externalPlansInScope, string planId, Structure structure, DoseValuePresentation doseValuePresentation, VolumePresentation volumePresentation)
        {
            return GetPlan(planId, ionPlansInScope, externalPlansInScope)?.GetDVHCumulativeData(structure, doseValuePresentation, volumePresentation, 0.001);
        }
        /// <summary>
        /// Gets the mean dose for the structure.
        /// </summary>
        /// <param name="structureId">Id for the structure.</param>
        /// <param name="planId">Id for the plan.</param>
        /// <returns>Mean dose in Gy</returns>
        public double GetMeanDoseForStructure(string structureId, string planId, DoseUnit doseValue)
        {
            double result = Double.NaN;

            Structure structure = GetStructureById(_context.IonPlansInScope, _context.ExternalPlansInScope, planId, structureId);

            if (structure != null)
            {
                DVHData data = GetDVHData(_context.IonPlansInScope, _context.ExternalPlansInScope, planId, structure, DoseValuePresentation.Absolute, VolumePresentation.Relative);
                if (data != null)
                {
                    result = GetDose(doseValue, data.MeanDose);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns the dose depended on the wanted dose dose unit. Ensure that the unit of the doseValue always matches the requested one.
        /// ONLY intended used for absolute dose values.
        /// </summary>
        /// <param name="requestedDoseUnit">The wanted dose unit [cGy,Gy,undefined]</param>
        /// <param name="doseValue">The actual dose value</param>
        /// <returns>The dose Value in the wanted dose unit.[cGy,Gy,NaN]</returns>
        public static double GetDose(DoseUnit requestedDoseUnit, DoseValue doseValue)
        {
            if (doseValue.IsUndefined() || Double.IsNaN(doseValue.Dose))
            {
                return Double.NaN;
            }
            else if (requestedDoseUnit == doseValue.Unit)
            {
                return doseValue.Dose;
            }
            else if (doseValue.IsAbsoluteDoseValue && requestedDoseUnit == DoseUnit.cGy && doseValue.Unit == DoseUnit.Gy)
            {
                return doseValue.Dose * 100.0;
            }
            else if (doseValue.IsAbsoluteDoseValue && requestedDoseUnit == DoseUnit.Gy && doseValue.Unit == DoseUnit.cGy)
            {
                return doseValue.Dose / 100.0;
            }
            else
            {
                return Double.NaN;
            }
        }
        /// <summary>
        /// Get the number of fractions for the selected plan.
        /// </summary>
        /// <param name="planId">Id of the plan.</param>
        /// <returns>Number of fractions for the plan.</returns>
        public int GetNumberOfFractions(string planId)
        {
            int numberOfFractions = -1;
            PlanSetup plan = GetPlan(planId, _context.IonPlansInScope, _context.ExternalPlansInScope);
            if (plan != null)
            {
                numberOfFractions = plan.NumberOfFractions.Value;
            }
            else
            {
                throw new Exception(String.Format("Could not find number of fractions for plan {0}.", planId));
            }
            return numberOfFractions;
        }
    }
}
