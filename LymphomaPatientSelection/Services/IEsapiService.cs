﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.TPS.Common.Model.Types;
using static VMS.TPS.Common.Model.Types.DoseValue;

namespace LymphomaPatientSelection.Services
{
    public interface IEsapiService
    {
        double GetMeanDoseForStructure(string structureId, string planId, DoseUnit doseValue);
        string GetPatientId();
        List<string> GetPlans();
        string GetPlanInFocus();
        List<string> GetStructures(string planId);
        int GetNumberOfFractions(string planId);
    }
}
