﻿namespace LymphomaPatientSelection.Services
{
    public interface IMessageBoxService
    {
        void ShowMessageBox(string message);
    }
}