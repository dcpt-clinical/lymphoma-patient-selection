﻿using LymphomaPatientSelection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public class RiskCalculatorExcess : RiskCalculator, IRiskCalculator
    {
        public RiskCalculatorExcess(List<LymphomaModelParameters> parametersList) : base (parametersList)
        {
            
        }
        /// <summary>
        /// Calculate the excess risk for Coronary Heart Diasease (CHD) depended on Mean Heart Dose (MHD) and Cardiac Risk Factors (CRF).
        /// </summary>
        /// <param name="CRF">Cardiac Risk Factors, CRF.</param>
        /// <param name="MHD">Mean Heart Dose, MHD [Gy].</param>
        /// <param name="age">Patient age [years]</param>
        /// <param name="gender">Patients gender [m = man or w = woman].</param>
        /// <returns>Excess risk for Coronary Heart Diasease, CHD [%].</returns>
        public double GetCHD(double CRF, double MHD, int age, char gender)
        {
            double result = double.NaN;

            if (CRF == 0.0)
            {
                result = 0.07 * MHD * GetParameter(age, gender, "CHD");
            }
            else if (CRF > 0.0)
            {
                result = 0.097 * MHD * GetParameter(age, gender, "CHD");
            }
            return result;
        }
        /// <summary>
        /// Calculate the excess risk for Heart Valve Disease (HVD) depended on Mean Aortic Heart Valve Dose (MAHVD) and Mean Mitral Heart Valve Dose (MMHVD).
        /// </summary>
        /// <param name="MAHVD">Mean Aortic Heart Valve Dose, MAHVD [Gy].</param>
        /// <param name="MMHVD">Mean Mitral Heart Valve Dose, MMHVD [Gy].</param>
        /// <param name="age">Patients age [years].</param>
        /// <param name="nFractions">Number of fractions.</param>
        /// <param name="gender">Patients gender [m = man or w = woman].</param>
        /// <returns>Excess risk for Heart valve Disease, HVD [%].</returns>
        public double GetHVD(double MAHVD, double MMHVD, int age, int nFractions, char gender)
        {
            double MHVD = Math.Max(MAHVD, MMHVD);
            return Math.Exp(-5.02) * MHVD * (((2.0 * nFractions) + MHVD) / (4.0 * nFractions)) * Math.Exp(0.075 * MHVD * (MHVD + (2.0 * nFractions)) / (4.0 * nFractions)) * GetParameter(age, gender, "HVD");
        }
        /// <summary>
        /// Calculate the excess risk for Lung Cancer (LC) depended on Mean Lung Dose (MLD).
        /// </summary>
        /// <param name="MLD">Mean Lung Dose, MLD [Gy].</param>
        /// <param name="age">Patients age [years].</param>
        /// <param name="gender">Patients gender [m = man or w = woman].</param>
        /// <returns>Excess risk for Lung Cancer, LC [%].</returns>
        public double GetLC(double MLD, int age, char gender)
        {
            return 0.141 * MLD * GetParameter(age, gender, "LC");
        }
        /// <summary>
        /// Calculate the excess risk for Breast Cancer (BC) depended on Mean Breast Dose (MBD).
        /// </summary>
        /// <param name="MBD">Mean Breast Dose, MBD [Gy].</param>
        /// <param name="age">Patients age [years].</param>
        /// <param name="gender">Patients gender [m = man or w = woman].</param>
        /// <returns>Excess risk for Breast Cancer, BC [%].</returns>
        public double GetBC(double MBD, int age, char gender)
        {
            double parameter = GetParameter(age, gender, "BC");

            if(Double.IsNaN(parameter) || Double.IsNaN(MBD))
            {
                return Double.NaN;
            }
            else
            {
                return 0.149 * MBD * parameter;
            }
        }
    }
}
