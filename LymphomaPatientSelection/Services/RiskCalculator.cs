﻿using LymphomaPatientSelection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public class RiskCalculator
    {
        List<LymphomaModelParameters> _parametersList;
        public RiskCalculator(List<LymphomaModelParameters> parametersList)
        {
            _parametersList = parametersList;
        }
        /// <summary>
        /// Get the correct parameter from the lymphoma model by the patients age, gender and choosen parameter.
        /// </summary>
        /// <param name="age">Patients age.</param>
        /// <param name="gender">Patients gender (m = man or w = woman)</param>
        /// <param name="parameter">Lymphoma model parameters: Coronary Heart Disease (CHD), Heart Valve Disease (HVD), Lung Cancer (LV) or Breast Cancer (BC).</param>
        /// <returns>The risk of getting one of the model diseases.</returns>
        public double GetParameter(int age, char gender, string parameter)
        {
            LymphomaModelParameters modelParameters = _parametersList.FirstOrDefault(p => p.Age == age);

            double result = double.NaN;

            if(modelParameters != null)
            {
                if(parameter == "CHD" && gender == 'm')
                {
                    result = modelParameters.CHDmen;
                }
                else if(parameter == "CHD" && gender == 'w')
                {
                    result = modelParameters.CHDwomen;
                }
                else if(parameter == "HVD")
                {
                    result = modelParameters.HVD;
                }
                else if(parameter == "LC" && gender == 'm')
                {
                    result = modelParameters.LCmen;
                }
                else if(parameter == "LC" && gender == 'w')
                {
                    result = modelParameters.LCwomen;
                }
                else if (parameter == "BC" && gender == 'w')
                {
                    result = modelParameters.BC;
                }
            }
            return result;
        }
    }
}
