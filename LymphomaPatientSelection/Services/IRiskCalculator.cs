﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public interface IRiskCalculator
    {
        double GetCHD(double CRF, double MHD, int age, char sex);
        double GetHVD(double MAHVD, double MMHVD, int age, int nFractions, char sex);
        double GetLC(double MLD, int age, char sex);
        double GetBC(double MBD, int age, char sex);
    }
}
