﻿namespace LymphomaPatientSelection.Services
{
    public interface ICPRService
    {
        int GetAge(string id);
        string GetCPRClean(string id);
        char GetGender(string id);
        string GetYear(string id);
        void SetCurrentTimeHelper(ICurrentTimeHelper helper);
    }
}