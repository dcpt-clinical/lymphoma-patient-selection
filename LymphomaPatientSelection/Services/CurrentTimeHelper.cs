﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public interface ICurrentTimeHelper
    {
        DateTime GetDateTimeNow();
    }
    public class CurrentTimeHelper : ICurrentTimeHelper
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now.Date;
        }
    }
}
