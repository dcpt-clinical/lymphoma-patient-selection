﻿using LymphomaPatientSelection.Models;
using LymphomaPatientSelection.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public static class LymphomaModelParametersIO
    {
        /// <summary>
        /// Reads the lymphoma values from the resource file.
        /// </summary>
        /// <returns>List of lymphoma model parameters.</returns>
        public static List<LymphomaModelParameters> ReadFile()
        {
            List<LymphomaModelParameters> lymphomaModelParameters = new List<LymphomaModelParameters>();

            string[] fileParts = Resources.LymfomaValues.Split('\r');

            for (int i = 0; i < fileParts.Length; i++)
            {
                LymphomaModelParameters lymphomaModel = new LymphomaModelParameters();

                if (i > 0)
                {
                    string[] lineParts = fileParts[i].Split(';');
                    lymphomaModel.Age = int.Parse(lineParts[0]);
                    lymphomaModel.CHDmen = double.Parse(lineParts[1]);
                    lymphomaModel.CHDwomen = double.Parse(lineParts[2]);
                    lymphomaModel.HVD = double.Parse(lineParts[3]);
                    lymphomaModel.LCmen = double.Parse(lineParts[4]);
                    lymphomaModel.LCwomen = double.Parse(lineParts[5]);
                    lymphomaModel.BC = double.Parse(lineParts[6]);

                    lymphomaModelParameters.Add(lymphomaModel);
                }
            }
            return lymphomaModelParameters;
        }
    }
}
