﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public class CPRService : ICPRService
    {
        private ICurrentTimeHelper _currentTimeHelper;

        public void SetCurrentTimeHelper(ICurrentTimeHelper helper)
        {
            _currentTimeHelper = helper;
        }
        public CPRService()
        {
            _currentTimeHelper = new CurrentTimeHelper();
        }
        /// <summary>
        /// Reformats the CPR number so it does not contain "-", has 10 digits, is all numbers and checks that the first 6 characters is a date.
        /// Throws exception if id does not contain exact 10 charaters or if all charaters is not numbers.
        /// </summary>
        /// <param name="id">Patient id number.</param>
        /// <returns>Cleaned CPR number.</returns>
        public string GetCPRClean(string id)
        {
            List<string> charsToRemove = new List<string>() { " ", "-" };

            string _id = id;

            charsToRemove.ForEach(ch => _id = _id.Replace(ch, string.Empty));

            if (_id.Length == 10 && _id.All(char.IsDigit) && DateTime.TryParse(_id.Substring(0, 6), out _) == true)
            {
                return _id;
            }
            else
            {
                throw new Exception("Patient id'et er ikke et dansk CPR nummer.\nIndtast selv køn og alder for patient.");
            }
        }
        /// <summary>
        /// Find out the gender of the patient from the last number in the patient id. Source: https://da.wikipedia.org/wiki/CPR-nummer and https://cpr.dk/cpr-systemet/opbygning-af-cpr-nummeret
        /// If even then women and if uneven the man.
        /// </summary>
        /// <param name="id">patients CPR number as a string.</param>
        /// <returns>m = man, w = woman and n = nonbinary/not known.</returns>
        public char GetGender(string id)
        {
            char result = 'n';
            char lastChar = id[id.Length - 1];
            if (Char.IsDigit(lastChar))
            {
                if (int.Parse(lastChar.ToString()) % 2 == 0)
                {
                    result = 'w';
                }
                else
                {
                    result = 'm';
                }
            }

            return result;
        }
        /// <summary>
        /// Finds the patients age from their CPR number depended on the patients birthday and the current date.
        /// </summary>
        /// <param name="id">Patients CPR number.</param>
        /// <returns>The age of the patient.</returns>
        public int GetAge(string id)
        {
            string _year = "";
            _year = GetYear(id);

            string date = id.Substring(0, 4) + _year;

            DateTime patientBirth = DateTime.ParseExact(date, "ddMMyyyy", null);
            DateTime currentdate = _currentTimeHelper.GetDateTimeNow();

            int _age = currentdate.Year - patientBirth.Year;
            
            if (DateTime.Compare(new DateTime(currentdate.Year,patientBirth.Month,patientBirth.Day), currentdate) < 0) //Patient birth date is before current date and therefor the patient has not yet turn 1 year older.
            {
                _age = _age - 1;
            }

            return _age;
        }
        /// <summary>
        /// Gets the patients birth year. Source: https://da.wikipedia.org/wiki/CPR-nummer and https://cpr.dk/cpr-systemet/opbygning-af-cpr-nummeret
        /// </summary>
        /// <param name="id">Patients CPR number.</param>
        /// <returns>The birth year.</returns>
        public string GetYear(string id)
        {
            int cif7 = int.Parse(id[6].ToString());
            int cif56 = int.Parse(id[4].ToString() + id[5].ToString());

            if (0 <= cif7 && cif7 <= 3)
            {
                return (1900 + cif56).ToString();
            }
            else if (cif7 == 4 && 0 <= cif56 && cif56 <= 36)
            {
                return (2000 + cif56).ToString();
            }
            else if (cif7 == 4 && 37 <= cif56 && cif56 <= 99)
            {
                return (1900 + cif56).ToString();
            }
            else if (5 <= cif7 && cif7 <= 8 && 0 <= cif56 && cif56 <= 57)
            {
                return (2000 + cif56).ToString();
            }
            else if (5 <= cif7 && cif7 <= 8 && 58 <= cif56 && cif56 <= 99)
            {
                return (1800 + cif56).ToString();
            }
            else if (cif7 == 9 && 0 <= cif56 && cif56 <= 36)
            {
                return (2000 + cif56).ToString();
            }
            else if (cif7 == 9 && 37 <= cif56 && cif56 <= 99)
            {
                return (1900 + cif56).ToString();
            }
            else
            {
                throw new Exception("Kunne ikke finde året for patientens CPR nummer.");
            }
        }
    }
}
