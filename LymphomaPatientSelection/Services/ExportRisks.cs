﻿using LymphomaPatientSelection.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public static class ExportRisks
    {
        //Exports the risk info
        public static void Export(string path, RiskInfo riskInfo)
        {
            using(Stream stream = new FileStream(path,FileMode.CreateNew))
            using(StreamWriter writer = new StreamWriter(stream,Encoding.UTF8))
            {
                writer.WriteLine(String.Format("Foton plan:;{0}",riskInfo.FotonPlan));
                writer.WriteLine(String.Format("Proton plan:;{0}", riskInfo.ProtonPlan));
                writer.WriteLine(String.Format("Patient age:;{0}years", riskInfo.PatientAge));
                writer.WriteLine(String.Format("Gender:;{0}", riskInfo.Gender));
                writer.WriteLine(String.Format("Number of fractions:;{0}", riskInfo.NumberOfFractions));
                writer.WriteLine(String.Format("CRF:;{0}", riskInfo.CRF));
                
                writer.WriteLine(String.Format(""));
                writer.WriteLine("Total Risk:");
                writer.WriteLine("Plan type;CHD;HVD;LC;BC");
                foreach (var t in riskInfo.TotalRisk)
                {
                    writer.WriteLine(String.Format("{0};{1};{2};{3};{4}", t.PlanType, t.CHD, t.HVD, t.LC, t.BC));
                }

                writer.WriteLine(String.Format(""));
                writer.WriteLine("Excess Risk:");
                writer.WriteLine("Plan type;CHD;HVD;LC;BC");
                foreach (var e in riskInfo.ExcessRisk)
                {
                    writer.WriteLine(String.Format("{0};{1};{2};{3};{4}", e.PlanType, e.CHD, e.HVD, e.LC, e.BC));
                }
            }
        }
    }

    public class RiskInfo
    {
        public string FotonPlan { get; set; }
        public string ProtonPlan { get; set; }
        public int PatientAge { get; set; }
        public string Gender { get; set; }
        public int NumberOfFractions { get; set; }
        public int CRF { get; set; }
        public List<RiskValues> TotalRisk { get; set; }
        public List<RiskValues> ExcessRisk { get; set; }
    }
}
