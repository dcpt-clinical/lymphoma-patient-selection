﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LymphomaPatientSelection.Services
{
    public class MessageBoxService : IMessageBoxService
    {
        /// <summary>
        /// Shows a messagebox
        /// </summary>
        /// <param name="message">The message that should be shown in the messagebox.</param>
        public void ShowMessageBox(string message)
        {
            System.Windows.MessageBox.Show(message);
        }
    }
}
